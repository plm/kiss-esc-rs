use super::*;

use paste::paste;

macro_rules! convert {
    ( $t:ty, $v:ty ) => {
        paste! {
        impl From<$v> for $t {
            fn from(value: $v) -> Self {
                Self(value)
            }
        }

        impl From<$t> for $v {
            fn from(value: $t) -> Self {
                value.0
            }
        }
        }
    };
}

convert!(Temperature, u8);
convert!(Voltage, u16);
convert!(Current, u16);
convert!(Consumption, u16);
convert!(Rpm, u16);
