use super::protocol::{Telemetry, TELEMETRY_LENGTH};

use core::mem;

use crc::{Crc, CRC_8_I_432_1};

pub type Bytes = [u8; TELEMETRY_LENGTH];

struct TelemetryBuffer {
    bytes: Bytes,
    offset: usize,
}

impl TelemetryBuffer {
    fn new() -> Self {
        Self {
            bytes: [0x0; TELEMETRY_LENGTH],
            offset: 0,
        }
    }

    fn put_u8(mut self, value: impl Into<u8>) -> Self {
        self.bytes[self.offset] = value.into();
        self.offset += mem::size_of::<u8>();
        self
    }

    fn put_u16(mut self, value: impl Into<u16>) -> Self {
        let end = self.offset + mem::size_of::<u16>();
        self.bytes[self.offset..end].copy_from_slice(&value.into().to_be_bytes());
        self.offset = end;
        self
    }

    fn checksum(mut self) -> Bytes {
        self.bytes[self.offset] =
            Crc::<u8>::new(&CRC_8_I_432_1).checksum(&self.bytes[..self.offset]);
        self.bytes
    }
}

pub trait TelemetryExt {
    fn serialize_telemetry(&self) -> Bytes;
}

impl TelemetryExt for Telemetry {
    fn serialize_telemetry(&self) -> Bytes {
        TelemetryBuffer::new()
            .put_u8(self.temperature)
            .put_u16(self.voltage)
            .put_u16(self.current)
            .put_u16(self.consumption)
            .put_u16(self.rpm)
            .checksum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::protocol::*;

    #[test]
    fn serialize_telemetry() {
        let telem = Telemetry::new(
            Temperature::from(99),
            Voltage::from(1200),
            Current::from(10),
            Consumption::from(6000),
            Rpm::from(200),
        );
        assert_eq!(
            telem.serialize_telemetry(),
            [0x63, 0x04, 0xB0, 0x00, 0x0A, 0x17, 0x70, 0x00, 0xC8, 0xB4]
        );
    }
}
