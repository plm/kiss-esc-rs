pub mod convert;

pub const TELEMETRY_LENGTH: usize = 10;

/// Temperature at 1° Celcius (C) resolution.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Temperature(u8);

/// Voltage at 0.01 volt (V) resolution.
///
/// A value of `1000` represents 10.00 V.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Voltage(u16);

/// Current at 0.01 ampere (A) resolution.
///
/// A value of `1000` represents 10.00 A.
/// ```
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Current(u16);

/// Consumption at 1 milliampere-hour (mAh) resolution.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Consumption(u16);

/// Electrical revolutions per minute (RPM) at 100 RPM resolution.
///
/// A value of `100` represents 10000 RPM.
///
/// To find the shaft RPM, divide by the motor's pole count divided by two.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Rpm(u16);

/// One transmission of telemetry data.
#[derive(Debug, PartialEq, Eq)]
pub struct Telemetry {
    pub temperature: Temperature,
    pub voltage: Voltage,
    pub current: Current,
    pub consumption: Consumption,
    pub rpm: Rpm,
}

impl Telemetry {
    pub fn new(
        temperature: Temperature,
        voltage: Voltage,
        current: Current,
        consumption: Consumption,
        rpm: Rpm,
    ) -> Self {
        Self {
            temperature,
            voltage,
            current,
            consumption,
            rpm,
        }
    }
}
