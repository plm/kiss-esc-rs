use super::protocol::*;

use core::fmt;

use crc::{Crc, CRC_8_I_432_1};
use winnow::{
    binary::{be_u16, be_u8},
    combinator::trace,
    error::{ContextError, ErrMode},
    stream::{AsBytes, Stream, StreamIsPartial},
    token::take,
    PResult, Parser, Partial,
};

type Output<O> = PResult<O>;

fn temperature<I>(i: &mut I) -> Output<Temperature>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("temperature", be_u8.map(Temperature::from)).parse_next(i)
}

fn voltage<I>(i: &mut I) -> Output<Voltage>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("voltage", be_u16.map(Voltage::from)).parse_next(i)
}

fn current<I>(i: &mut I) -> Output<Current>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("current", be_u16.map(Current::from)).parse_next(i)
}

fn consumption<I>(i: &mut I) -> Output<Consumption>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("consumption", be_u16.map(Consumption::from)).parse_next(i)
}

fn rpm<I>(i: &mut I) -> Output<Rpm>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("rpm", be_u16.map(Rpm::from)).parse_next(i)
}

fn telemetry<I>(i: &mut I) -> Output<(usize, Telemetry)>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("telemetry", move |i: &mut I| {
        let checkpoint = i.checkpoint();
        let t = temperature.parse_next(i)?;
        let v = voltage.parse_next(i)?;
        let c = current.parse_next(i)?;
        let m = consumption.parse_next(i)?;
        let r = rpm.parse_next(i)?;
        let length = i.offset_from(&checkpoint);
        i.reset(&checkpoint);
        let checkpoint = i.checkpoint();
        let data = take(length).parse_next(i)?;
        let data = data.as_bytes();
        let _ = be_u8
            .verify(|&v| v == Crc::<u8>::new(&CRC_8_I_432_1).checksum(data))
            .parse_next(i)?;
        Ok((i.offset_from(&checkpoint), Telemetry::new(t, v, c, m, r)))
    })
    .parse_next(i)
}

#[derive(Debug, PartialEq)]
pub struct InvalidTelemetry(ContextError);

#[derive(Debug, PartialEq)]
pub enum Error {
    Incomplete,
    InvalidTelemetry(InvalidTelemetry),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Parsing error: {:?}", self)
    }
}

trait TelemetryExt {
    fn parse_telemetry(self) -> Result<(usize, Telemetry), Error>;
}

impl TelemetryExt for &[u8] {
    fn parse_telemetry(self) -> Result<(usize, Telemetry), Error> {
        telemetry
            .parse_next(&mut Partial::new(self))
            .map_err(|err| match err {
                ErrMode::Incomplete(_) => Error::Incomplete,
                ErrMode::Backtrack(e) | ErrMode::Cut(e) => {
                    Error::InvalidTelemetry(InvalidTelemetry(e))
                }
            })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const SAMPLE_DATA: [u8; TELEMETRY_LENGTH] =
        [0x63, 0x04, 0xB0, 0x00, 0x0A, 0x17, 0x70, 0x00, 0xC8, 0xB4];

    #[test]
    fn parse_valid_data() {
        if let Ok((read, telem)) = (&SAMPLE_DATA[..]).parse_telemetry() {
            assert_eq!(read, 10);
            assert_eq!(telem.temperature, Temperature::from(99));
            assert_eq!(telem.voltage, Voltage::from(1200));
            assert_eq!(telem.current, Current::from(10));
            assert_eq!(telem.consumption, Consumption::from(6000));
            assert_eq!(telem.rpm, Rpm::from(200));
        } else {
            unreachable!();
        }
    }

    #[test]
    fn parse_partial_data() {
        for len in 0..SAMPLE_DATA.len() {
            assert_eq!(
                (&SAMPLE_DATA[..len]).parse_telemetry(),
                Err(Error::Incomplete)
            );
        }
    }

    #[test]
    fn parse_invalid_data() {
        let data = [0x0; TELEMETRY_LENGTH];
        assert!(matches!(
            (&data[..]).parse_telemetry(),
            Err(Error::InvalidTelemetry(_))
        ));
    }
}
